/**
 * 
 */
package net.ijt.geom2d;

/**
 * @author dlegland
 *
 */
public interface Transform2D
{
	public Point2D transform(Point2D point);
}
